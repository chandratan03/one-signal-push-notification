package com.jelres.onesignal.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("one-signal")
public class OneSignalPushNotificationController {

  @PostMapping("/sendMessageToUser/{userId}/{message}")
  public void sendMessageToUser(@PathVariable("userId") String userId,
      @PathVariable("message") String message)
  {
    PushNotificationOptions.sendMessageToUser(message, userId);
  }
}
