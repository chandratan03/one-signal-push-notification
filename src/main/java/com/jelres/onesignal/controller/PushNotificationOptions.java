package com.jelres.onesignal.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class PushNotificationOptions {
  public static final String REST_API_KEY = "REST_API_KEY";
  public static final String APP_ID = "__APP_ID";

  private static String mountResponseRequest(HttpURLConnection con, int httpResponse) throws IOException {
    String jsonResponse;
    if (  httpResponse >= HttpURLConnection.HTTP_OK
        && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
      Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
      jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
      scanner.close();
    }
    else {
      Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
      jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
      scanner.close();
    }
    return jsonResponse;
  }

  public static void sendMessageToUser(
      String message, String userId) {
    try {
      String jsonResponse;

      URL url = new URL("https://onesignal.com/api/v1/notifications");
      HttpURLConnection con = (HttpURLConnection)url.openConnection();
      con.setUseCaches(false);
      con.setDoOutput(true);
      con.setDoInput(true);

      con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
      con.setRequestProperty("Authorization", "Basic " + REST_API_KEY);
      con.setRequestMethod("POST");

      String strJsonBody = "{"
          +   "\"app_id\": \""+ APP_ID +"\","
          +   "\"name\": {\"en\": \""+message+"\"},"
          +   "\"contents\": {\"en\": \""+ message +"\"},"
          +   "\"headings\": {\"en\": \""+message+"\"},"
          +   "\"include_subscription_ids\": [\""+ userId +"\"]"
          + "}";


      System.out.println("strJsonBody: " + strJsonBody);

      byte[] sendBytes = strJsonBody.getBytes(StandardCharsets.UTF_8);
      con.setFixedLengthStreamingMode(sendBytes.length);

      OutputStream outputStream = con.getOutputStream();
      outputStream.write(sendBytes);

      int httpResponse = con.getResponseCode();
      System.out.println("httpResponse: " + httpResponse);

      jsonResponse = mountResponseRequest(con, httpResponse);
      System.out.println("jsonResponse: " + jsonResponse);

    } catch(Throwable t) {
      t.printStackTrace();
    }
  }


}
